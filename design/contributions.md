0987# Creative Commons Contributions

## 3D-Assets
 - [Antique Dresser](https://sketchfab.com/3d-models/boy-room-19035fc846034003b76e2914ba6dd7a6) by [Darren McNerney 3D](https://sketchfab.com/DarrenMcnerney3D) CC-BY
 - [Wardrobe - Low poly](https://skfb.ly/ow6TJ) by [ixMkc1](https://sketchfab.com/ixMkc1) CC-BY
 - [Simple Bean Bag](https://sketchfab.com/3d-models/simple-bean-bag-64d111cec3d842f09cbb8c68b1e49c8d) by [AleixoAlonso](https://sketchfab.com/AleixoAlonso) CC-BY
 - [Cobwebs](https://skfb.ly/ouu9C) by [DJMaesen](https://sketchfab.com/bumstrum) CC-BY
 - [Laptop](https://skfb.ly/6RVFt) by [Aullwen](https://sketchfab.com/Aullwen) CC-BY
 - [Home Speaker Bar](https://skfb.ly/oqZnY) by [re1monsen] CC-BY
 - [Designer Storage Box 2](https://sketchfab.com/3d-models/designer-storage-box-2-db1275e988484908afcf44d0487aed69) by [Raphael Escamilla](https://sketchfab.com/Raffey) CC-BY
 - [Carpet Fluffy](https://sketchfab.com/3d-models/carpet-fluffy-435d64ef1e36457790000c6b6aa4b8b2) by [polybEAN](https://sketchfab.com/polybean) CC-BY
 - [backpacks blockout homework](https://sketchfab.com/3d-models/backpacks-blockout-homework-e80686790d884b21b52f2308e51af9c6) by [Fellming](https://sketchfab.com/Fellm) CC-BY

## Textures from polyhaven.com
- [Rough Wood](https://polyhaven.com/a/rough_wood) by [Rob Tuytel](https://polyhaven.com/all?a=Rob%20Tuytel) CC-0
- [Brick Wall 001](https://polyhaven.com/a/brick_wall_001) by [Dimitrios Savva](https://polyhaven.com/all?a=Dimitrios%20Savva)

## Textures from AmbientCG.com
 - [Fabric 030](https://ambientcg.com/view?id=Fabric030)
 - [Wood Substance 009](https://ambientcg.com/view?id=WoodSubstance009)
 - [Wicker 010 B](https://ambientcg.com/view?id=Wicker010B)
 
## Comic Illustrations

 - by [Fernand0FC](https://www.deviantart.com/fernand0fc), CC-BY-3.0
	 + [Corporate level bodyguard](https://www.deviantart.com/fernand0fc/art/Corporate-level-bodyguard-803579124)
	 + [Into the glowing sea](https://www.deviantart.com/fernand0fc/art/Into-the-glowing-sea-834238466)
	 + [Anti-Hero](https://www.deviantart.com/fernand0fc/art/Anti-Hero-555054767)

## Music
 - [Foundations I by Azure Studios](https://azurestudios.bandcamp.com/album/foundations-i-24bit), CC-BY 3.0
 - [10 Ambient RPG Tracks](https://alkakrab.itch.io/free-10-rpg-game-ambient-tracks-music-pack-no-copyright) by [alkakrab](https://alkakrab.itch.io/) *"Absolutely Free For Commercial use."*

## Sounds Effects
 - [Rain on Windows, Interior](https://freesound.org/people/InspectorJ/sounds/346641/) by [InspectorJ](https://freesound.org/people/InspectorJ/) CC-BY 3.0
 - [Rain and thunder](https://freesound.org/people/MrAuralization/sounds/241294/) by [MrAuralization](https://freesound.org/people/MrAuralization/) CC-BY 3.0
 - [Heavy Thunder Strike - no Rain - QUADRO](https://freesound.org/people/BlueDelta/sounds/446753/) by [BlueDelta](https://freesound.org/people/BlueDelta/) CC-BY 3.0
 
## Fonts all using OFL

 - Nanum Script by [Sandoll](https://fonts.google.com/?query=Sandoll)
 - Atkinson Hyperlegible by Braille Institute, Applied Design Works, Elliott Scott, Megan Eiswerth, Linus Boman, Theodore Petrosky
 - 