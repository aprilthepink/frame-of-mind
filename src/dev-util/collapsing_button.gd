@tool
extends Button

func hide():
    if visible == true:
        var tween:Tween = create_tween()
        custom_minimum_size = get_minimum_size()
        var tmp = text
        text = ""
        tween.tween_property(self, "custom_minimum_size", Vector2(size.x, 0), 0.2)
        update_minimum_size()    
        await tween.finished
        visible = false
        text = tmp
        update_minimum_size()
        
func show():
    if visible == false:
        var tmp = text
        var tween:Tween = create_tween()    
        tween.tween_property(self, "custom_minimum_size", get_minimum_size(), 0.2)
        text = ""
        update_minimum_size()    
        visible = true
        await tween.finished
        text = tmp
    
