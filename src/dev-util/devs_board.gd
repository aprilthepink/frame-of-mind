extends Control

func get_cards_by_scene_id(id: int) -> Array:
    var output:Array
    
    var scene = get_child(id)
    
    for i in range(scene.get_child_count()):
        output.append(scene.get_child(i))
        for post in output[i].get_children():
            if post is PostIt:
                output[i].remove_child(post)
        
    for card in output:
        card.transform = Transform3D()
        for postIt in card.own_postits:
            postIt.transform = Transform3D()
    
    return output

func get_cards_by_name_array(names: Array) -> Dictionary:
    var output:Dictionary = {
        "cards": [],
        "postIts": []
    }
    
    for scene in get_children():
        for card in scene.get_children():
            for postIt in card.get_children():
                if names.has(postIt.name):
                    postIt.transform = Transform3D()
                    output['postIts'].append(postIt)
            
            if names.has(card.name):
                card.transform = Transform3D()
                output['cards'].append(card)
                for child in card.get_children():
                    if child is PostIt:
                        card.remove_child(child)
    return output
