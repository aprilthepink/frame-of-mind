extends Area3D

@onready var pass_to_actor = $UiWrapper/UiSprite/SubViewport/Collectable_ui
@onready var wrapper = $UiWrapper
@onready var ui = $UiWrapper/UiSprite/SubViewport.get_child(0)

var revealed: bool = false:
    set(on_stage):
        revealed = on_stage
        if on_stage:
            wrapper.show()
            ui.show()
        else:
            wrapper.hide()
            ui.hide()
            
var has_mouse: bool = false
        
# Called when the node enters the scene tree for the first time.
func _ready():
    connect("mouse_entered", Callable(self, "_on_mouse_entered"))

func _on_mouse_entered():
    input_ray_pickable = false
    ui.collapsed = false
    has_mouse = true
        
func _on_mouse_exited():
    print("mouse_exited")
    input_ray_pickable = true
    ui.collapsed = true
    has_mouse = false

func reveal():
    revealed = true

func collapse():
    _on_mouse_exited()
    revealed = false
