@tool

extends Area2D
class_name Card
var compatible_postits: Array[PostIt] = []
var own_postits: Array[PostIt] = []
var wiggle_pos: float = 0
var wiggle_intensity: float = 0
var noise: Noise = FastNoiseLite.new()
var wiggle_tween
var scale_tween

@export var text: String = "" :
    set(value):
        text = value
        if get_children() != [] or Engine.is_editor_hint():
            $Label.text = value
            $BackgroundSprite.frame = text.hash() % $BackgroundSprite.sprite_frames.get_frame_count($BackgroundSprite.animation)
        if !Engine.is_editor_hint():
            wiggle_pos = float(text.hash() % 100)
            _handle_wiggle(0)
@export var wiggle_strength: float = 0.2
@export var wiggle_speed: float = 5
@export_range(1, 2) var scale_bump: float = 1.05
@export_range(1.0, 10.0) var bounce_speed: float = 5
@export var highlighted: bool = false:
    set(highlight):
        if highlight != highlighted:
            highlighted = highlight

            if is_inside_tree() and is_node_ready():
                if scale_tween: scale_tween.kill()
                if wiggle_tween: wiggle_tween.kill()
                if highlighted:
                    scale_tween = get_tree().create_tween()
                    scale_tween.tween_property(self, "scale", Vector2(scale_bump, scale_bump), 0.1)
                    wiggle_tween = get_tree().create_tween()
                    wiggle_tween.tween_property(self, "wiggle_intensity", 1, 0.2)
                else:
                    scale_tween = get_tree().create_tween()
                    scale_tween.tween_property(self, "scale", Vector2(1, 1), 0.3)
                    wiggle_tween = get_tree().create_tween()
                    wiggle_tween.tween_property(self, "wiggle_intensity", 0, 0.5)
            else:
                if highlighted:
                    scale = Vector2(scale_bump, scale_bump)
                    wiggle_intensity = 1
                else:
                    scale = Vector2(1,1)
                    wiggle_intensity = 0
                    
@export var voice_line: AudioStream = null
@export var is_dragable: bool = false
@onready var diameter = $CollisionShape2D.shape.height

var is_dragged: bool = false:
    set(dragged):
        is_dragged = dragged
        z_index = int(dragged)

var is_mouse_entered: bool = false
var mouse_offset: Vector2

func _ready():
    
    _handle_wiggle(0)
    if not Engine.is_editor_hint() and is_inside_tree():
        for postit in self.get_children():
            if postit is PostIt: self.own_postits.append(postit as PostIt)
        
        for postit in get_tree().get_nodes_in_group(name):
            if postit is PostIt: self.compatible_postits.append(postit as PostIt)
    
        compatible_postits.append_array(own_postits)
        
        if own_postits.size() == 2:
            own_postits[0].sibling = own_postits[1]
            own_postits[1].sibling = own_postits[0]
        
    $BackgroundSprite.frame = text.hash() % $BackgroundSprite.sprite_frames.get_frame_count($BackgroundSprite.animation)
    $Label.text = self.text
    
    wiggle_pos = float(text.hash() % 100)
    if not Engine.is_editor_hint():
        _handle_wiggle(0)

func _process(delta: float) -> void:
    if highlighted:
        _handle_wiggle(delta)
    
    if get_overlapping_areas().size() > 0 and is_dragable:
        for area in get_overlapping_areas():
            if area is Card or area is CardCollider:
                if area is CardCollider:
                    position += area.direction * delta
                elif not (area.highlighted or self.highlighted) and area.is_dragable: 
                    var diff:Vector2 = position - area.position
                    position -= diff.normalized() * ((diff.length()-diameter)/diameter) * bounce_speed * (delta/(1.0/60))
    
    _move_card()

func get_text() -> String:
    return $Label.text

func _handle_wiggle(delta):
    wiggle_pos += delta * wiggle_speed * wiggle_intensity
    
    rotation = noise.get_noise_1d(wiggle_pos)*wiggle_strength

func replace_with(card: Card):
    self.text = card.text
    self.compatible_postits = card.compatible_postits
    self.own_postits = card.own_postits
    self.voice_line = card.voice_line
    self.name = card.name
    
func _on_focus_entered():
    print(self, "is focused")

func _on_focus_exited():
    print(self, "is not focused")

func _on_mouse_entered():
    is_mouse_entered = true
    if not Input.is_action_pressed("mouse_left"):
        if has_postit_attached():
            if get_child(-1).highlighted:
                return
        highlighted = true
        if "handle_hover" in owner:
            owner.handle_hover(self)

func _on_mouse_exited():
    highlighted = false
    is_mouse_entered = false

func _on_input_event(viewport, event, shape_idx):
    
    if event is InputEventMouseMotion:
        _move_card()
        
    if event is InputEventMouseButton:
        if event.button_index == MOUSE_BUTTON_LEFT:
            if "handle_mouse_button" in owner:
                mouse_offset = (get_viewport().get_mouse_position() - position)                
                if highlighted:
                    owner.handle_mouse_button(self, event)

func _move_card():
    if is_dragged:
        position += (get_viewport().get_mouse_position() - position) - mouse_offset

func has_postit_attached() -> bool:
    var all_children = get_children()
    for child in all_children:
        if child is PostIt:
            return true
    return false

func check_hover():
    if is_mouse_entered:
        _on_mouse_entered()
