extends Control

const dev_board_pre = preload("res://dev-util/board of devs.tscn")
var dev_board: Control

func _ready():
    dev_board = dev_board_pre.instantiate()

    if $cards.get_child_count(false) > 0:
        $cards.get_children(false)[0].grab_focus()

    # Testing code
    for item in dev_board.find_children("*"):
        if item is Card:
            spawn_card((item as Card).duplicate())
        elif item is PostIt:
            spawn_postit((item as PostIt).duplicate())

func _process(delta: float):
    pass

func spawn_card(card: Card):
    $cards.add_child(card)

    if $cards.get_child_count(false) == 1:
        $cards.get_children(false)[0].grab_focus()

    populate_focus_neighbors()

func spawn_postit(postit: PostIt):
    $postits.add_child(postit)

    populate_focus_neighbors()

func populate_focus_neighbors():
    # TODO reorder cards based on position

    if $cards.get_child_count(false) <= 0:
        return

    var first_card = $cards.get_children(false)[0]
    var first_postit = $postits.get_children(false)[0] if $postits.get_child_count(false) > 0 else first_card

    var first_board_card = $mindmap.get_children(false)[0] if $mindmap.get_child_count(false) > 0 else first_card

    var cards = $cards.get_children(false) as Array[Card]
    for i in cards.size():
        var card = cards[i]
        if card == first_card or not (card is Card):
            continue
        card.focus_neighbor_right = first_board_card # FIXME should be a valid focusable object, but it refuses
        card.focus_neighbor_left = first_postit
        card.focus_neighbor_up = cards[(i - 1) % cards.size()]
        card.focus_neighbor_down = cards[(i + 1) % cards.size()]

    var postits = $postits.get_children(false) as Array[PostIt]
    for i in postits.size():
        var postit = postits[i]
        if not (postit is PostIt):
            continue
        postit.focus_neighbor_right = first_card
        postit.focus_neighbor_left = first_board_card
        postit.focus_neighbor_up = postits[(i - 1) % postits.size()]
        postit.focus_neighbor_down = postits[(i + 1) % postits.size()]

    var board_items = $mindmap.get_children(false) as Array
    for i in board_items.size():
        var board_item = board_items[i]
        board_item.focus_neighbor_right = first_postit
        board_item.focus_neighbor_left = first_card
        board_item.focus_neighbor_up = board_items[(i - 1) % board_items.size()]
        board_item.focus_neighbor_down = board_items[(i + 1) % board_items.size()]
