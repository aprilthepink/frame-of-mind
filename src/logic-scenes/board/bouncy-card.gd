extends Area2D

@export_range(1.0, 10.0) var bounce_speed: float = 5

@onready var diameter = $CollisionShape2D.shape.radius * 2

# Called when the node enters the scene tree for the first time.
func _ready():
    pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
    if get_overlapping_areas().size() > 0:
        for area in get_overlapping_areas():
            var diff:Vector2 = position - area.position
            position -= diff.normalized() * ((diff.length()-diameter)/diameter) * bounce_speed * (delta/(1.0/60))
