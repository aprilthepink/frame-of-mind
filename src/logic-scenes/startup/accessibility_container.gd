extends TabContainer

var focus_list: Array

func _ready():
	for child in get_children():
		focus_list.append(_find_selectable_in(child))

func _find_selectable_in(parent:Control):
	if parent.focus_mode != FOCUS_NONE: 
		return parent
	if parent.get_child_count() == 0: 
		return self
	else:
		for child in parent.get_children():
			var ret = _find_selectable_in(child)
			if not ret == self:
				return ret
	
func _unhandled_input(event):
	
	if event.is_action_type():
		if event.is_action_pressed("ui_left"): previous()
		elif event.is_action_pressed("ui_right"): next()

func next():
	if current_tab < get_tab_count()-1:
		if !Input.is_action_just_released("mouse_left"):
			if not get_viewport().gui_get_focus_owner() == null:
				focus_list[current_tab] = get_viewport().gui_get_focus_owner()
			current_tab += 1
			focus_list[current_tab].grab_focus()
		else:
			current_tab += 1
	
func previous():
	if current_tab > 0:
		if !Input.is_mouse_button_pressed(MOUSE_BUTTON_LEFT):
			if not get_viewport().gui_get_focus_owner() == null:
				focus_list[current_tab] = get_viewport().gui_get_focus_owner()
			current_tab -= 1
			focus_list[current_tab].grab_focus()
		else:
			current_tab -= 1
