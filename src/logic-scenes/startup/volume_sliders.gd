extends HSlider

@export var audio_bus_id: int = 0

func _on_value_changed(volume_lin: float):
    AudioServer.set_bus_volume_db(audio_bus_id, linear_to_db(volume_lin))
