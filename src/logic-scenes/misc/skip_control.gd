extends Control

signal skip
@export var skip_delay: float = 0.5
@export var costum_owner: NodePath
var time_pressed: float = 0
@onready var button: Button = $"Skip Button"
@onready var progress: ProgressBar = $ProgressBar
var pressed: bool

func _ready():
    owner = get_node(costum_owner)
    
    if owner == null:
        owner = get_parent().get_parent()
        
    owner.connect("visibility_changed", Callable(self, "owner_visibility_changed"))
    visible = owner.visible

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
    if pressed and visible:
        time_pressed += delta
        progress.value = time_pressed / skip_delay
        if time_pressed >= skip_delay:
            emit_signal("skip")
            pressed = false
            time_pressed = 0
            
func _input(event):
    if visible:
        if event.is_action_pressed("skip"):
            pressed = true
        elif event.is_action_released("skip"):
            pressed = false
            time_pressed = 0
    
func owner_visibility_changed():
    visible = owner.visible

func _on_skip_button_toggled(button_pressed):
    if button_pressed:
        pressed = true
    else:
        pressed = false
        time_pressed = 0
