extends Panel

@onready var has_stage = true:
    set(focus):
        if focus:
            has_stage = State.request_focus(self)
        else:
            has_stage = false            
            State.drop_own_focus(self)
