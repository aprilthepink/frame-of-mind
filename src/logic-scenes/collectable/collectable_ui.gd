@tool
extends CenterContainer
class_name Collectable_Ui

@export var scene = Scenes.id

@export var collapsed = true:
    set(collapse):
        if is_inside_tree() and not Engine.is_editor_hint():
            if State.reduce_motion:
                collapsed = false
                return
        if collapse and not collapsed:
            if is_inside_tree():
                _hide_buttons()
            collapsed = collapse
        elif not collapse and collapsed:
            if is_inside_tree():
                _show_buttons()
            collapsed = collapse
            
        if collapse and has_stage: State.leave_stage(self)
            
@export var is_story: bool = false:
    set(story):
        is_story = story
        if not story:
            $Panel/Content/Buttons/VBoxContainer/collect_or_listen.text = "Order Thoughts"
@export var has_stage: bool = false:
    set(focused):
        print("set focus of card to ", focused)
        
        if has_stage == focused: return    
        
        if focused:
            has_stage = true
            collapsed = false
            if not visible: show()
            $Panel/Content/Buttons/VBoxContainer/collect_or_listen.grab_focus()
        elif has_stage:
            has_stage = false
            get_viewport().gui_release_focus()
        
@export var collected: bool = false:
    set(set_collected):
        collected = set_collected
        if set_collected:
            $Panel/Content/Buttons/VBoxContainer/put_back.show()
            if is_story:
                $Panel/Content/Buttons/VBoxContainer/put_back.disabled = true
                $Panel/Content/Buttons/VBoxContainer/collect_or_listen.text = "listen again"
                if State.allow_skipping:
                    $Panel/Content/Buttons/VBoxContainer/skip.text = "discard cards (skip)"
            else:
                $Panel/Content/Buttons/VBoxContainer/collect_or_listen.disabled = true
            $Panel/Content/Buttons/VBoxContainer/put_back.show()
        else: 
            $Panel/Content/Buttons/VBoxContainer/collect_or_listen.disabled = false
            
@export var skipped: bool = false

@export var item_name: String = "":
    set(new_name):
        item_name = new_name
        if is_inside_tree():
            $Panel/Content/Name.text = new_name

@export var content_notes: String = "":
    set(new_notes):
        content_notes = new_notes
        if is_inside_tree():
            $Panel/Content/ContentNotes.text = new_notes

signal card_collected
signal open_board
signal scene_skipped(i: int)

# Called when the node enters the scene tree for the first time.
func _ready():
    #$Panel/Content/ContentNotes.visible = State.show_content_notes
    #$Panel/Content/Buttons/VBoxContainer/Summary.visible = State.provide_summaries
    #$Panel/Content/Buttons/VBoxContainer/skip.visible = State.allow_skipping
    if visible and not collapsed: _show_buttons()
    
    item_name = item_name
    content_notes = content_notes
    is_story = is_story

func _hide_buttons():
    if is_inside_tree():
        if not State.reduce_motion: $AnimationPlayer.play_backwards("show_buttons")
    
func _show_buttons():
    if is_inside_tree():
        if not State.reduce_motion:
            $AnimationPlayer.play("show_buttons")
        else:
            $AnimationPlayer.play("RESET")
    else:
        $AnimationPlayer.play("RESET")
    
func hide():
    if visible:
        _hide_buttons()
        var tween = create_tween()
        tween.tween_property(self, "modulate", 0, 0.4)
        _hide_buttons()
        await tween.finished
        visible = false
        if has_stage: State.leave_stage(self)

func show():
    if !visible:
        $Panel/Content/ContentNotes.visible = State.show_content_notes
        $Panel/Content/Buttons/VBoxContainer/skip.visible = State.allow_skipping and is_story and not skipped
        if not collapsed:
            _show_buttons()
        modulate = Color()
        visible = true
        var tween = create_tween()
        tween.tween_property(self, "modulate", Color(1, 1, 1), 0.4)

func _yoink_focus():
    if not has_stage:
        State.transition_stage_to(self)
        
func _on_pick_button_pressed():
    print("card collected!")
    if scene != null:
        State.leave_stage(self)
        get_tree().call_group("animation_player", "play_scene", scene, collected)
        if skipped: emit_signal("scene_skipped", -1)
        collected = true
    else:
        emit_signal("open_board")
    hide()


func _on_skip_pressed():
    print("Scene skipped!")
    if scene != null:
        emit_signal("scene_skipped", 1)
        skipped = true
        $Panel/Content/Buttons/VBoxContainer/collect_or_listen.text = "collect (un-skip)"
    
    State.leave_stage(self)
